import setuptools

setuptools.setup(
    name="code-red-bbehm",
    version="0.0.3",
    author="Brian Behm",
    author_email="brian.behm@gmail.com  ",
    description="Creates a code red message from the provided text",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)