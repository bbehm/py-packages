def code_red(msg) -> str:
    return f"Code Red! - {msg}"


# a little redder than using code_red()
def code_redder(msg) -> str:
    return f"Code Redder! - {msg}"


# really red!
def code_really_red(msg) -> str:
    return f"Code Really Red!!! - {msg}"

# changed red