def code_green(msg) -> str:
    return f"Code Green! - {msg}"


# a little greener than code_green()
def code_greener(msg) -> str:
    return f"Code Green!!! - {msg}"


# really green!
def code_really_green(msg) -> str:
    return f"Code Really Green!!! - {msg}"

# changed green